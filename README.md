## full_oppo6771_18611-user 9 PPR1.180610.011 eng.root.20200915.143154 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: RMX1831
- Brand: realme
- Flavor: cipher_RMX1831-userdebug
- Release Version: 12
- Id: SD1A.210817.036.A8
- Incremental: eng.neolit.20211127.063655
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SD1A.210817.015.A4/7697517:user/release-keys
- OTA version: 
- Branch: full_oppo6771_18611-user-9-PPR1.180610.011-eng.root.20200915.143154-release-keys-random-text-267271994023283
- Repo: realme_rmx1831_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
